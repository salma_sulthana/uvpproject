using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace UVP_Vicroads_project
{
    public class Tests
    {
        //creating refrance 
        IWebDriver driver = new ChromeDriver("F:\\chromedriver2");

        [SetUp]
        public void Setup()
        {
            
            // Navigating
            driver.Navigate().GoToUrl("https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit");
        }

        [Test]
       public void Test1()
        {
            //Vechile Type
            Step1_0f_7.SelectDropeDown(driver, "ph_pagebody_0$phthreecolumnmaincontent_0$panel$VehicleType$DDList", "Passenger vehicle", "Name");

            //passenger Vechile Type
            Step1_0f_7.SelectDropeDown(driver, "ph_pagebody_0$phthreecolumnmaincontent_0$panel$PassengerVehicleSubType$DDList", "Sedan", "Name");

            //Address
            Step1_0f_7.EnterText(driver, "ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown", "26 Bridgewater Way,Truganina Vic 3029", "Id");
          
            Step1_0f_7.SelectDropeDown(driver, "ph_pagebody_0$phthreecolumnmaincontent_0$panel$PermitDuration$DDList", "16 days", "Name");
            Step1_0f_7.Click(driver, "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext", "Id");

            bool IsTestElementPresent(IWebDriver driver)
            {
                try
                {
                    driver.FindElement(By.XPath("//*[text() ='Step 2 of 7 : Select permit type']"));
                    
                    return true;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }

            
            Assert.Pass();
        }

        [TearDown]

        public void CloseUp()
        {
            driver.Close();
        }

        

    
        
    }
}